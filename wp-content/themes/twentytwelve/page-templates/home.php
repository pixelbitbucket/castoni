<?php
/**
 * Template Name: Strona home castoni
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

do_action( 'get_header', null );


$templates = array();
$templates[] = 'home-header.php';

$lang = get_locale();
$homeData = array(
    'first-line' => array(
        'pl_PL' => 'RADOŚĆ JEST WAŻNA!',
        'en_US' => 'JOY IS IMPORTANT!',
    ),
    'second-line' => array(
        'pl_PL' => 'A SZCZYPTA CZARÓW NIE ZASZKODZI.',
        'en_US' => 'A PINCH OF MAGIC DOES NOT HURT.',
    ),
    'description' => array(
        'pl_PL' => '
Od zawsze ważne było dla mnie światło.<br/>
Słońce o różnych porach dnia wydobywa ze świata zupełnie inne szczegóły.<br/><br/>
Światło – prawda, energia, piękno.<br/>
Oddziaływuje na nasze zmysły, wpływa na nastawienie.<br/><br/>
W sztuce, która najbardziej płodna jest gdy towarzyszą jej silne emocje,<br />
na ogół te negatywne, ja od zawsze poszukiwałam dzieł powstałych w pełni<br />
szczęścia, tych które szczęście obrazują i przekazują.<br/><br/>
Odkąd zorientowałam się co chce w życiu robić marzyłam o własnej firmie<br />
odzieżowej. Skończyłam projektowanie odzieży, zbierałam doświadczenia,<br/>
ale zabrakło wtedy odwagi. Wierzę, że nic nie dzieje się jednak bez przyczyny.<br/>
Musiało wydarzyć się jeszcze wiele w moim życiu aby nadszedł ten moment.<br/><br/><br/>
<span class="idea-bigger">„Wszystko jest możliwe. Niemożliwe wymaga po prostu więcej czasu”.</span><br/>
                                                <span class="f-right desc-right-text">Dan Brown</span>
                                                <div class="clearfix"></div><br/>
Nadszedł ten moment, zapraszam Cię do mojego świata, gdzie krok po kroku<br />
realizuję moje marzenia, aby przekazać Ci trochę radości i wewnętrznego słońca.<br/><br/>
Bez przerwy staram się rozwijać, uczyć, poznawać, doświadczać, podróżować,<br />
udowadniać samej sobie, że potrafię. Wciąż uczę się wiary w siebie.<br/><br/>
Lubię projektować, lubię tworzyć, lubię działać.<br/>
Lubię dostrzegać „magię”.<br/>
Lubię stare i nowe, emocje, duszę, spokój, radość, harmonie, równowagę.<br/><br/>
<p class="idea-bigger abracadabra">Czary – mary</p><br/>
Marzysz? Na pewno o czymś marzysz…<br/>
Nigdy nie rezygnuj ze swoich marzeń!<br/><br/>
<span class="f-right desc-right-text">…to dopiero początek</span><br/>
<div class="clearfix"></div>
<span class="f-right desc-right-text"><img src="/wp-content/themes/twentytwelve/images/podpis.png" alt="Anna Zapart" /></span>
<div class="clearfix"></div><br/>
                   ',
        'en_US' => '
Light has always been important to me.<br/>
The sun, at different times of the day, highlights various details from the world.<br/><br/>

Light – truth, energy, beauty.<br/>
It affects our senses, influences our attitude.<br/><br/>

In art, which is the most prolific when accompanied by strong emotions -<br/>
especially those negative ones, I have always been looking for the works of art<br/>
created in the full happiness, those that illustrate and communicate joy.<br/><br/>

Since the moment when I realized what I want to do in my life, I have always<br/>
dreamt about my own clothing company.<br/>
I graduated from fashion design, gained experience, but I lacked courage.<br/>
However, I believe that nothing happens without a reason.<br/>
A lot of things had to happen in my life so that the moment could come.<br/><br/><br/>

<span class="idea-bigger">“ Everything is possible. The impossible just takes longer.”</span><br/>

<span class="f-right desc-right-text">Dan Brown</span>
                                                <div class="clearfix"></div><br/>

Now is the moment, I would like to invite You to my world, where step by step<br/>
I fulfill my dreams to give You a little joy and some internal light.<br/><br/>

I have been constantly trying to develop, learn, discover, experience, travel,<br/>
and prove myself that I can. I’m still learning to believe in myself.<br/><br/>

I like to design, I like to create, I like to work<br/>
I like to discern the magic.<br/>
I like the old and the new, emotions, soul, peace, joy, harmony, balance.<br/><br/>

<p class="idea-bigger abracadabra">Abracadabra ; )</p><br/>

Do you have dreams? You must definitely dream about something…<br/>
Never let go of Your dreams!<br/><br/>

<span class="f-right desc-right-text">…it’s just the beginning</span>
<div class="clearfix"></div>
<span class="f-right desc-right-text"><img src="/wp-content/themes/twentytwelve/images/podpis.png" alt="Anna Zapart" /></span>
<div class="clearfix"></div><br/>


                    ',
    ),
    'product-location' => array(
        'pl_PL' => 'W TYCH WYJĄTKOWYCH<br /> MIEJSCACH ZNAJDZIESZ<br /> NASZE PRODUKTY',
        'en_US' => 'IN THESE AMAZING<br /> PLACES YOU WILL FIND<br /> OUR PRODUCTS',
    ),
    'contact-info' => array(
        'pl_PL' => 'CHCESZ SIĘ Z NAMI<br />SKONTAKTOWAĆ?<br />ZAPRASZAMY!',
        'en_US' => 'WOULD YOU LIKE<br /> TO CONTACT US?<br /> FEEL FREE TO E-MAIL!',
    )
);

// Backward compat code will be removed in a future release
if ('' == locate_template($templates, true))
    load_template( ABSPATH . WPINC . '/theme-compat/home-header.php');
?>

<div id="primary" class="site-content home-page">
    <div id="content" role="main">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'content', 'page' ); ?>
        <?php endwhile; // end of the loop. ?>

        <div class="separator-home"></div>
        <div class="separator-home"></div>
        <div id="idea" class="separator-home"></div>

        <p class="font-big"><?php echo $homeData['first-line'][$lang]; ?></p>
        <p class="font-medium <?php echo $lang; ?>"><?php echo $homeData['second-line'][$lang]; ?></p>

        <div class="separator-home"></div>
        <div class="separator-home"></div>
        <div class="separator-home"></div>

        <div class="super-leaf">
            <img src="/wp-content/themes/twentytwelve/images/leaf2.jpg" />
        </div>
        <div class="home-description"><?php echo $homeData['description'][$lang]; ?></div>



        <div id="lookbook">
            <div class="separator-home"></div>
            <div class="separator-home"></div>
            <?php include(ABSPATH . '/wp-content/themes/twentytwelve/projects.php'); ?>
        </div>

        <div class="separator-home"></div>
        <div id="sklepy" class="separator-home"></div>

        <p class="font-big"><?php echo $homeData['product-location'][$lang]; ?></p>

        <div class="separator-home"></div>
        <div class="separator-home"></div>

        <div id="map"></div>

        <div class="separator-home"></div>

        <div  class="contact-contaner font-medium">
            <div class="contact-row">
                <div class="contact-item">
                    <p>GESZEFT<br/>Katowice<br/>ul. Morcinka 23-25</p>
                </div>
                <div class="contact-item">
                    <p>MARKA<br/>Kraków<br/>ul. Józefa 5</p>
                </div>
                <div class="contact-item">
                    <p>MAGAZYN WYSOKI<br/>ŁÓDŹ<br/>ul. Piotrkowska 138-140</p>
                </div>
                <div class="contact-item">
                    <p>PRACOWNIA FRYZJERSKA<br/>AGNIESZKA MARCIŃCZAK<br/>Bytom<br/>ul. Jagielońska 15</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="contact-row-more">
                <div class="contact-item">
                    <p> MANDALA<br/>Opole<br/>ul. Kościuszki 31</p>
                </div>
                <div class="contact-item">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="contact-arrow-container">
                <div class="contact-arrow down"></div>
            </div>
        </div>

        <div class="separator-home"></div>
        <div class="separator-home"></div>
        <div class="separator-home"></div>


        <p class="font-big"><?php echo $homeData['contact-info'][$lang]; ?></p>

        <div class="separator-home"></div>

        <div id="kontakt">
            <p class="">INFO@CASTONI.COM</p>
            <p class="contact-facebook"><a href=" https://www.facebook.com/CASTONI-557413447609718/?ref=hl" title="facebook">facebook</a> / instagram</p>
        </div>


        <div class="separator-home"></div>
        <div class="separator-home"></div>
        <div class="separator-home"></div>

        <div class="back-arrow-container">
            <a href="#top" class="anchor-link"><div class="back-arrow up"></div></a>
        </div>



    </div><!-- #content -->

 <script type="text/javascript">
     function initialize() {
         var locations = [];

         locations.push(['<div style="padding: 10px;">Katowice</div>', 50.268326, 19.0175091, 3]);
         locations.push(['<div style="padding: 10px;">Kraków</div>', 50.0503502,19.9415128, 3]);
         locations.push(['<div style="padding: 10px;">Łódź</div>', 51.7612308, 19.4562704, 3]);
         locations.push(['<div style="padding: 10px;">Bytom</div>', 50.3442242, 18.9159631, 3]);

         var map = new google.maps.Map(document.getElementById('map'), {
             zoom: 6,
             scrollwheel: false,
             center: new google.maps.LatLng(52, 20),
             mapTypeId: google.maps.MapTypeId.ROADMAP
         });

         var infowindow = new google.maps.InfoWindow();

         var marker, i;

         for (i = 0; i < locations.length; i++) {
             marker = new google.maps.Marker({
                 position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                 map: map,
                 icon: '/wp-content/uploads/images/gmap_marker.png'
             });

             google.maps.event.addListener(marker, 'click', (function(marker, i) {
                 return function() {
                     infowindow.setContent(locations[i][0]);
                     infowindow.open(map, marker);
                 }
             })(marker, i));
         }
     }

     function loadScript() {
         var script = document.createElement('script');
         script.type = 'text/javascript';
         script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
             'callback=initialize';
         document.body.appendChild(script);
     }

     window.onload = loadScript;

     jQuery(document).ready(function(){
         jQuery('.contact-arrow').click(function(){
             jQuery(this).toggleClass('up');
             jQuery('.contact-row-more').slideToggle(600);
         });

         jQuery('.home-menu-toggle').click(function(){
             jQuery(this).toggleClass('active');

             jQuery('.home-menu-container').slideToggle(600);
         })
     });
 </script>

</div><!-- #primary -->
<?php
do_action( 'get_footer', $name );

$templates = array();

$templates[] = 'home-footer.php';

// Backward compat code will be removed in a future release
if ('' == locate_template($templates, true))
    load_template( ABSPATH . WPINC . '/theme-compat/home-footer.php'); ?>
