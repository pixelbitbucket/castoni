<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	return;
}

$lang = get_locale();

$tdata = array(
    'pl_PL' => 'WYSYŁKA',
    'en_US' => 'SHIPPING',
);

?>
<div class="shipping-info">
    <div class="title"><?php echo $tdata[$lang]; ?></div>
    <div class="shipping-content">
        <p class="sh"> <?php echo $tdata[$lang]; ?><span class="close">x</span></p>

        <p>PRZESYŁKA POLECONA POCZTĄ POLSKĄ (płatność z góry) - 8zł</p>

        <p>PRZESYŁKA POLECONA POCZTĄ POLSKĄ (pobranie) - 17zł</p>

        <p>PRZESYŁKA KURIERSKA DPD (płatność z góry) - 16zł</p>

        <p>PRZESYŁKA KURIERSKA DPD (pobranie) - 24zł</p>

        <p>PRZESYŁKA BEZPŁATNA (zamówienie powyżej 200zł)</p>

        <p>Odbiór osobisty w Bytomiu lub Bielsku-Białej po wcześniejszym uzgodnieniu.</p>
    </div>
</div>
<div itemprop="description">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery('.shipping-info .title').click(function(){
            jQuery('.shipping-info .shipping-content').slideToggle(600);
        });

        jQuery('.shipping-info .close').click(function(){
            jQuery('.shipping-info .shipping-content').slideToggle(600);
        });
    });
</script>
