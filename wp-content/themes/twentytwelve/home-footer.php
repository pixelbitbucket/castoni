<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>



</div><!-- #main .wrapper -->
<footer id="colophon" role="contentinfo">
    <div class="footer-container font-small">
        <p class="home-footer-link f-left">info@castoni.com</p>
        <a href="https://www.facebook.com/CASTONI-557413447609718/?ref=hl"><div class="facebook-icon footer-icon"></div></a>
        <a href="#"><div class="pinterest-icon footer-icon"></div></a>
        <a href="#"><div class="instagram-icon footer-icon"></div></a>

        <p class="f-right">Copyright <?php echo date('Y'); ?> CASTONI / Anna Zapart</p>
        <div class="clearfix"></div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->
</div> <!-- #content-wrap -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.scrollbox.min.js"></script>
</body>
</html>