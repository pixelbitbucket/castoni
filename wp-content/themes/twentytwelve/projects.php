<?php include 'project_data.php'; ?>
<?php
$number = array(1 => 'one', 2 => 'two', 3 => 'three', 0 => 'four');


$projectList = array(
    1 => array(
        'title' => '&nbsp;',
        'description' => '&nbsp;',
        'photos' => 5,
    ),
    2 => array(
        'title' => '&nbsp;',
        'description' => '&nbsp;',
        'photos' => 4,
    ),
//    3 => array(
//        'title' => '&nbsp;',
//        'description' => '&nbsp;',
//        'photos' => 5,
//    ),
//    4 => array(
//        'title' => '&nbsp;',
//        'description' => '&nbsp;',
//        'photos' => 5,
//    ),
);
$indx = 1; $projectCount = count($projectList);

?>

<div class="separator"></div>



<?php foreach($projectList as $id => $project): ?>

    <?php echo $id % 4 == 1 ? '<div class="row-container">' : ''?>
    <div class="gallery-item item-<?php echo 2014+$id;?> <?php echo $number[$id%4]; ?>"
         id="item-<?php echo $indx;?>"
         data-gallery-id="<?php echo $id;?>"
         data-gallery-number="<?php echo $indx;?>"
         data-gallery-count="<?php echo $projectCount;?>"
        >
        <div class="hover-item"></div>
        <img class="preview" src="wp-content/uploads/images/lookbook/<?php echo $id + 2014 . '_mini.jpg'; ?>" alt="Gallery-Preview" />

        <div class="links hidden">
            <ul>
                <?php for($index = 1; $index < $project['photos'] + 1; $index++): ?>
                    <li><img src="wp-content/uploads/images/lookbook/<?php echo $id + 2014 . '_duze' . $index . '.jpg' ;?>" alt="image" /></li>
                <?php endfor; ?>
            </ul>
        </div>
        <div class="project-info-wrapper hidden">
            <div class="info">
                <p class="title"><?php echo $project['title']; ?></p>
                <p class="desc"><?php echo $project['description']; ?></p>
            </div>
            <div class="project-nav">
                <div class="icon close"></div>
                <div class="icon up <?php echo $indx == 1 ? 'inactive' : 'active'; ?>"></div>
                <div class="icon down  <?php echo $indx == $projectCount ? 'inactive' : 'active'; ?>"></div>
                <div class="icon next"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?php $indx++; ?>
    <?php echo $id%4 == 0 ? '</div>' : ''?>
<?php endforeach; ?>

<?php /*  !!!!!  remove this after remove empty boxes  !!!!! */;?>

<div class="gallery-item-empty"></div>
<div class="gallery-item-empty"></div>


<?php echo count($projectList)%4 != 0 ? '</div>' : ''?>

<div style="clear: both;"></div>
<div class="separator"></div>

<script type="text/javascript">
    var galleyContainerHtml = '<div id="gallery-wrapper" class="gallery-wrapper">' +
        '<div id="project-info-wrapper"></div>' +
        '<div id="gallery-container" class="scroll-img gallery-container">' +
        '</div></div>';
    var currentId = null;




    jQuery('.gallery-item').click(function(){
        // actual item clicked
        var $actual = jQuery(this);

        jQuery('.gallery-item').removeClass('active');
        $actual.addClass('active');

        // remove old gallery
        jQuery('#gallery-wrapper').remove();

        // insert empty container
        $actual.after(galleyContainerHtml);

        var $galleryContainer = jQuery('#gallery-container');
        var $infoContainer = jQuery('#project-info-wrapper');

        // add title and desc to new container
        $infoContainer.append($actual.find('.project-info-wrapper').html());

        // add content to new container
        $galleryContainer.append($actual.find('.links').html());

        $galleryContainer.scrollbox({
            direction: 'h',
            switchItems: 1,
            onMouseOverPause: false,
            autoPlay: true,
            delay: 4
        });

        jQuery('.project-nav .close').click(function(){
            // remove old gallery
            jQuery('#gallery-wrapper').remove();
            jQuery('.gallery-item').removeClass('active');

        });

        jQuery('.project-nav .up.active').click(function(){

            var index = $actual.data('gallery-number');
            if (index < 2) {
                return;
            }

            jQuery('#item-' + --index).click();
        });

        jQuery('.project-nav .down.active').click(function(){

            var index = $actual.data('gallery-number');
            if (index >= $actual.data('gallery-count')) {
                return;
            }

            jQuery('#item-' + ++index).click();
        });

        jQuery('.project-nav .next').click(function(){
            $galleryContainer.trigger('forward');
        });
    });
</script>